<?php

namespace RoobieBoobieee\Teams;

use RoobieBoobieee\Teams\Section;

class Message implements \JsonSerializable
{
  /**
  * The text content of the message.
  *
  * @var array
  */
  private $data;

  public function __construct(string $summary, $color = '0076D7')
  {
    $this->data = [
      '@type' => 'MessageCard',
      '@context' => 'http://schema.org/extensions',
      'themeColor' => $color,
      'summary' => $summary,
      'sections' => [],
    ];
  }

  public function add(Section $item)
  {
    $this->data['sections'][] = $item;
  }

  public function jsonSerialize()
  {
    return $this->data;
  }
}

<?php

namespace RoobieBoobieee\Teams;

use Illuminate\Notifications\Notification;

class TeamsChannel
{
    protected $teams;

    public function __construct(Teams $teams)
    {
        $this->teams = $teams;
    }

    /**
     * Send the given notification.
     *
     * @param mixed $notifiable
     * @param \Illuminate\Notifications\Notification $notification
     *
     * @throws \RoobieBoobieee\Teams\Exceptions\CouldNotSendNotification
     */
    public function send($notifiable, Notification $notification)
    {
        if (! $webhook = $notifiable->routeNotificationFor('teams')) {
            return;
        }

        if (! $message = $notification->toTeams($notifiable)) {
            return;
        }

        if (! $message instanceof \RoobieBoobieee\Teams\Message) {
            throw new \RuntimeException('Message has incorrect type.');
        }

        return $this->teams->send($webhook, $message);
    }
}

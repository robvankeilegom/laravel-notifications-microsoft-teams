<?php

namespace RoobieBoobieee\Teams\Interfaces;

interface ContainerItem
{
    public function type();
}

<?php

namespace RoobieBoobieee\Teams;

use Illuminate\Support\Arr;

class Fact implements \JsonSerializable
{

  private $name;

  private $value;

  public function __construct($name = null, $value = null) {
    $this->name = $name;
    $this->value = $value;
  }


  public function name(string $data = null)
  {
    if ($data === null) {
      return $this->name;
    }

    $this->name = $data;
  }


  public function value(string $data = null)
  {
    if ($data === null) {
      return $this->value;
    }

    $this->value = $data;
  }

  public function jsonSerialize()
  {
    return [
      'name' => $this->name,
      'value' => $this->value,
    ];
  }
}
